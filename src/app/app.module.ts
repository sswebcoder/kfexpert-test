import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ApartmentsModule } from './apartments/apartments.module';
import { HttpClientModule } from '@angular/common/http';
import { SplashLoaderComponent } from './components/splash-loader/splash-loader.component';

@NgModule({
  declarations: [
    AppComponent,
    SplashLoaderComponent
  ],
  imports: [
    BrowserModule,
    ApartmentsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
