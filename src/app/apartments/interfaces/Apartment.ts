import { IPrice } from './Price';

export interface IApartment {
    street: string;
    short_description: string;
    rooms: number;
    price: IPrice;
    images: string[];
    url_id: string;
    address: string;
    level: string;
    rooms_count_from: string;
    area_from: string;
}
