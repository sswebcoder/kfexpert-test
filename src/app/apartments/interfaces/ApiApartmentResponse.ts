import { IApartment } from './Apartment';

export interface IApartmentResponse {
    status: 'success' | 'error';
    data: {
        lot: IApartment;
    };
}
