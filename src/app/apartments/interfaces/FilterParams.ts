export interface IFilterParams {
    [param: string]: string;
    bedrooms_count_from: string;
    bedrooms_count_to: string;
    area_from: string;
    area_to: string;
}
