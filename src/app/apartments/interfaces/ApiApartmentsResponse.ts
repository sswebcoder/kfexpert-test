import { IApartment } from './Apartment';

export interface IApiApartmentsResponse {
    data: {
        count: number;
        status: 'success' | 'error';
        message?: string;
        lots: IApartment[];
    };
}
