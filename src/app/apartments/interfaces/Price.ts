export interface IPrice {
    chf?: number;
    eur?: number;
    gbp?: number;
    rur?: number;
    usd?: number;
}
