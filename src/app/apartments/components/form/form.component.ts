import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SplashLoaderService } from '../../../components/splash-loader/splash-loader.service';
import { ApartmentsService } from '../../services/apartments.service';
import { Observable } from 'rxjs';
import { IApartment } from '../../interfaces/Apartment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private splashService: SplashLoaderService,
    private apartmentsService: ApartmentsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(({ id }) => this.loadData(id));
  }

  private loadData(id: string): void {
    this.splashService.loading = true;
    this.apartmentsService.getApartment(id).subscribe(() => this.splashService.loading = false);
  }

  public get apartment(): Observable<IApartment> {
    return this.apartmentsService.apartment;
  }

}
