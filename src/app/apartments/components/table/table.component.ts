import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApartmentsService } from '../../services/apartments.service';
import { Observable } from 'rxjs';
import { IApartment } from '../../interfaces/Apartment';
import { SplashLoaderService } from '../../../components/splash-loader/splash-loader.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public filterForm = new FormGroup({
    area_from: new FormControl(null),
    area_to: new FormControl(null),
    bedrooms_count_from: new FormControl(null),
    bedrooms_count_to: new FormControl(null),
    floor_from: new FormControl({
      value: null,
      disabled: true
    }),
    floor_to: new FormControl({
      value: null,
      disabled: true
    }),
  });

  constructor(
    private apartmentsService: ApartmentsService,
    private splashService: SplashLoaderService,
  ) { }

  ngOnInit() {
    this.splashService.loading = true;
    this.apartmentsService.getApartments().subscribe(() => this.splashService.loading = false);
  }

  public onFilterSubmit(e: Event): void {
    e.preventDefault();
    this.splashService.loading = true;
    this.apartmentsService.filterApartments(this.filterForm.getRawValue()).subscribe();
  }

  public get apartments(): Observable<IApartment[]> {
    return this.apartmentsService.apartments;
  }

}
