import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApartmentsService } from '../../services/apartments.service';
import { Observable, Subscription } from 'rxjs';
import { SplashLoaderService } from '../../../components/splash-loader/splash-loader.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  private _countSubscription: Subscription;
  constructor(
    private apatmentsService: ApartmentsService,
    private splashService: SplashLoaderService
  ) { }

  ngOnInit() {
    this.splashService.loading = true;
    this._countSubscription = this.apatmentsService.getCount().subscribe(() => this.splashService.loading = false);
  }

  ngOnDestroy() {
    this._countSubscription.unsubscribe();
  }

  public get count(): Observable<number> {
    return this.apatmentsService.count;
  }

}
