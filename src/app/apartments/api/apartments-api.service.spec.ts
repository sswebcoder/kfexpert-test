import { TestBed } from '@angular/core/testing';
import { ApartmentsApiService } from './apartments-api.service';
import { HttpClientModule } from '@angular/common/http';

describe('ApartmentsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]
  }));

  it('should be created', () => {
    const service: ApartmentsApiService = TestBed.get(ApartmentsApiService);
    expect(service).toBeTruthy();
  });
});
