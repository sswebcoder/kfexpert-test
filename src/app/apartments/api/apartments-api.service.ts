import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IApiApartmentsResponse } from '../interfaces/ApiApartmentsResponse';
import { IApartment } from '../interfaces/Apartment';
import { IFilterParams } from '../interfaces/FilterParams';
import { IApartmentResponse } from '../interfaces/ApiApartmentResponse';

@Injectable({
  providedIn: 'root'
})
export class ApartmentsApiService {
  private _apartmentsUrl = 'https://kf.expert/api/v1/property?query=';
  private _apartmentUrl = 'https://kf.expert/api/v1/lots/';

  constructor(
    private http: HttpClient,
  ) { }

  public getCount(): Observable<number> {
    return this.http.get<IApiApartmentsResponse>(this._apartmentsUrl)
      .pipe(map(response => response.data.count));
  }

  public getApartments(filterParams?: IFilterParams): Observable<IApartment[]> {
    const params = new HttpParams({
      fromObject: filterParams
    });
    return this.http.get<IApiApartmentsResponse>(this._apartmentsUrl, { params })
      .pipe(map(response => response.data.lots));
  }

  public getApartment(id: string): Observable<IApartment> {
    const url = `${this._apartmentUrl}${id}`;
    return this.http.get<IApartmentResponse>(url)
      .pipe(map(response => response.data.lot));
  }

}
