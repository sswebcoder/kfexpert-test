import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './components/main/main.component';
import { TableComponent } from './components/table/table.component';
import { FormComponent } from './components/form/form.component';
import { routing } from './routing';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [
    MainComponent,
    TableComponent,
    FormComponent
  ],
  exports: [
    RouterModule,
  ]
})
export class ApartmentsModule { }
