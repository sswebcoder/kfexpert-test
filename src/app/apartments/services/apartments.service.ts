import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ApartmentsApiService } from '../api/apartments-api.service';
import { IApartment } from '../interfaces/Apartment';
import { IFilterParams } from '../interfaces/FilterParams';

@Injectable({
  providedIn: 'root'
})
export class ApartmentsService {
  private _count: Subject<number> = new Subject();
  public count = this._count.asObservable();

  private _apartments: Subject<IApartment[]> = new Subject();
  public apartments = this._apartments.asObservable();

  private _apartment: Subject<IApartment> = new Subject();
  public apartment = this._apartment.asObservable();

  constructor(
    private api: ApartmentsApiService,
  ) { }

  public getCount(): Observable<number> {
    this.api.getCount().subscribe(count => this._count.next(count));
    return this.count;
  }

  public getApartments(): Observable<IApartment[]> {
    this.api.getApartments().subscribe(apartments => this._apartments.next(apartments));
    return this.apartments;
  }

  public filterApartments(filterParams: IFilterParams): Observable<IApartment[]> {
    this.api.getApartments(filterParams).subscribe(apartments => this._apartments.next(apartments));
    return this.apartments;
  }

  public getApartment(id: string): Observable<IApartment> {
    this.api.getApartment(id).subscribe(apartment => this._apartment.next(apartment));
    return this.apartment;
  }
}
