import { TestBed } from '@angular/core/testing';
import { ApartmentsService } from './apartments.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('ApartmentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      RouterTestingModule
    ],
  }));

  it('should be created', () => {
    const service: ApartmentsService = TestBed.get(ApartmentsService);
    expect(service).toBeTruthy();
  });
});
