import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './components/table/table.component';
import { FormComponent } from './components/form/form.component';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
    { path: 'apartments', component: TableComponent },
    { path: 'apartments/:id', component: FormComponent },
    { path: '', component: MainComponent },
    { path: '**', redirectTo: '/apartments' },
];

export const routing = RouterModule.forRoot(routes);
