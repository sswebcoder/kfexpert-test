import { TestBed } from '@angular/core/testing';

import { SplashLoaderService } from './splash-loader.service';

describe('SplashLoaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SplashLoaderService = TestBed.get(SplashLoaderService);
    expect(service).toBeTruthy();
  });
});
