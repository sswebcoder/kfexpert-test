import { Component, OnInit } from '@angular/core';
import { SplashLoaderService } from './splash-loader.service';

@Component({
  selector: 'app-splash-loader',
  templateUrl: './splash-loader.component.html',
  styleUrls: ['./splash-loader.component.scss']
})
export class SplashLoaderComponent implements OnInit {

  constructor(
    private service: SplashLoaderService
  ) { }

  ngOnInit() {
  }

  public get loading(): boolean {
    return this.service.loading;
  }
}
