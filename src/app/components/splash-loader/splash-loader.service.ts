import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SplashLoaderService {
  private _loading = false;
  public get loading(): boolean {
    return this._loading;
  }

  public set loading(value: boolean) {
    if (this._loading === true && value === false) {
      setTimeout(() => this._loading = false, 1000);
    } else {
      this._loading = value;
    }
  }
  constructor() { }
}
